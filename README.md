MobileApp16 - Game Server
===

To every group will be assigned an __APIKey__ (e.g. 327bf040-a02f-4300-89a7-b2c43b195dc9).

The server is organized in Rooms.
In each room players will be matched randomly.
Each Group will be able to access the __public__  room and a private one called __groupXX__ where XX is the number of the group.

# Headers

Every HTTP request __must__ contain an __authorization__ header formatted as follow:
```
APIKey key
```
where key is your __APIKey__.

If the __authorization__ is missing or malformed you will obtain a __401 Unauthorized__ error.

# Long Polling
All GET APIs follow a Long Polling approach.

You need to set a long timeout (suggested 30000 ms) to your request.

If data is available the request will complete correctly.

If the request times out you should start a request again.

# APIs
All the api return __200__ on success in any other case an error code will be returned.
The error message format is as follow:
```json
{
    "error": "Kind of error",
    "message": "Description of the error"
}
```

__GET__: http://mobileapp16.bernaschina.com/api/room/:roomname

This request will put you in the queue for a game in this room.

Results:
 * Timeout try again
 * 401 You cannot access this room
 * 200 Game created
```json
{
    "game": "the id of the game",
    "color": "your color black/white",
    "url": "url to use during this game"
}
```

__GET__: game url (returned by http://mobileapp16.bernaschina.com/api/room/:roomname)

This request will give you your opponent move.
The game will be terminated if a player doesn't call this endpoint for more than 60 seconds.

Results:
 * Timeout try again
 * 401 You cannot access this game
 * 410 Game is terminated
 * 200 Your opponent did a move
```json
{
    "move": "the encoded move, it may be a non valid move",
}
```

__POST__: game url (returned by http://mobileapp16.bernaschina.com/api/room/:roomname)

Send move to your opponent.

Results:
 * Timeout try again
 * 401 You cannot access this game
 * 403 It is not your turn
 * 200 Move sent

Content:

the body of the message __must__ contain the encoded move (e.g. M1342).

See project specifications for move encoding.

__DELETE__: game url (returned by http://mobileapp16.bernaschina.com/api/room/:roomname)

Ends a game

The body of the message __must__ contain why it has been terminated (e.g. M1342).

Suggested:
 * terminated
 * invalid move
 * abandon

# Example usage
This example is in pseudo-code.

```javascript
// waiting for game match
var game;
while (true) {
    // do the request
    var result = do_get_request('http://mobileapp16.bernaschina.com/api/room/public');
    // if request timedout restart request
    if (!result.isTimedout()) {
        // if result is ok
        if (result.status == 200) {
            // get game description
            game = result.body;
            break;
        } else {
            // terminate if error
            exit(result.body.error);
        }
    }
}

// if I'm first
if (game.color == 'white') {
    // send move
    var result = do_post_request(game.url, encoded_move);
    if (result.status != 200) {
        exit(result.body.error);
    }
}
// start game loop
while (true) {
    // wait for opponent
    var result = do_get_request(game.url);
    if (!result.isTimedout()) {
        // if result is ok
        if (result.status == 200) {
            // get game description
            opponent_encoded_move = result.body.move;
            //...
            // ask user for move
            //...
            if (game_terminated) {
                // send move
                var result = do_delete_request(game.url, termination_reason);
                if (result.status != 200) {
                    exit(result.body.error);
                }
                break;
            } else {
                // send move
                var result = do_post_request(game.url, encoded_move);
                if (result.status != 200) {
                    exit(result.body.error);
                }
            }
        } else {
            // terminate if error
            exit(result.body.error);
        }
    }
}

```
