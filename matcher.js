/*jslint node: true, nomen: true */
"use strict";

var EventEmitter = require("events").EventEmitter,
    uuid = require('uuid/v4');

function generateAccessPoints() {
    var ap1 = uuid(),
        ap2;
    do {
        ap2 = uuid();
    } while (ap2 === ap1);
    return {
        '0': ap1,
        '1': ap2
    };
}

function formatAccessPoint(req, game, ap) {
    return 'http://' + req.hostname +
        '/api/game/' + game +
        '/ap/' + ap;
}

function isAlive(ts) {
    return Date.now() - ts < 60000;
}

function createUserMatcher(options) {
    if (!options.games) { throw new Error('missing games property'); }
    if (!(options.games instanceof EventEmitter)) { throw new Error('games must be an EventEmitter'); }

    var games = {},
        rooms = {};

    setInterval(function () {
        Object.keys(games).forEach(function (id) {
            var game = games[id];
            if (game.death) {
                if (game.delete_at < Date.now()) {
                    options.games.emit('deleted', game);
                }
            } else if (!(isAlive(game.accessed[0]) && isAlive(game.accessed[1]))) {
                game.death = 'timeout';
                options.games.emit('terminated', game);
            }
        });
    }, 10000);

    options.games.on('deleted', function (game) {
        delete games[game.id];
    });

    function generateId() {
        var id;
        do {
            id = uuid();
        } while (games[id]);
        return id;
    }

    return function (req, res) {
        if (rooms[req.params.room]) {
            var game = {
                id: generateId(),
                turn: 0,
                aps: generateAccessPoints(),
                data: {},
                callbacks: {},
                accessed: {
                    '0': Date.now(),
                    '1': Date.now()
                }
            };

            games[game.id] = game;

            rooms[req.params.room](game);
            rooms[req.params.room] = undefined;

            res.json({
                game: game.id,
                color: 'black',
                url : formatAccessPoint(req, game.id, game.aps[1])
            });

            options.games.emit('created', game);
        } else {
            rooms[req.params.room] = function (game) {
                res.json({
                    game: game.id,
                    color: 'white',
                    url : formatAccessPoint(req, game.id, game.aps[0])
                });
            };
            res.setTimeout(30000);
            res.on('timeout', function () {
                rooms[req.params.room] = undefined;
            });
            req.once('aborted', function () {
                rooms[req.params.room] = undefined;
            });
        }
    };
}

exports.createUserMatcher = createUserMatcher;
