/*jslint node: true, nomen: true */
"use strict";

var EventEmitter = require('events').EventEmitter,
    createRouter = require('express').Router,
    bodyParser = require('body-parser'),
    createUserMatcher = require('./matcher').createUserMatcher,
    game = require('./game'),
    apikey = require('./apikey');

function createAPI(options) {
    if (!options.access) { throw new Error('missing access property'); }
    if (typeof options.access !== 'object') { throw new Error('access must be a object'); }
    var games = new EventEmitter(),
        router = createRouter();

    games.on('created', function (game) {
        console.log('Game ' + game.id + ' created');
    });
    games.on('terminated', function (game) {
        console.log('Game ' + game.id + ' terminated (' + game.death + ')');
    });
    games.on('deleted', function (game) {
        console.log('Game ' + game.id + ' deleted');
    });

    router.use(apikey.parseAuthentication);
    router.use(apikey.createAPIKeyValidityChecker(options.access));

    router.get('/room/:room',
        apikey.createAPIKeyRoomAccessChecker(options.access),
        createUserMatcher({games: games}));

    router.use('/game/:game/ap/:ap', game.createGameParser({games: games}));

    router.get('/game/:game/ap/:ap', game.gameReporter);
    router.post('/game/:game/ap/:ap', bodyParser.text(), game.gameRunner);
    router.delete('/game/:game/ap/:ap', bodyParser.text(), game.createGameEnder({games: games}));

    return router;
}

exports.createAPI = createAPI;
