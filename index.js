/*jslint node: true */
"use strict";

var express = require('express'),
    morgan = require('morgan'),
    createAPI = require('./api').createAPI,
    access = require('./access');

var app = express();

app.set('trust proxy', true);

app.use(morgan('combined'));
app.use('/api', createAPI({access: access}));

app.listen(3000, function () {
    console.log('Server listening on port 3000!');
});
