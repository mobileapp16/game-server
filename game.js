/*jslint node: true, nomen: true */
"use strict";

var _ = require('lodash'),
    EventEmitter = require("events").EventEmitter,
    isUUID = require('validator').isUUID,
    uuid = require('uuid/v4');

function createGameParser(options) {
    if (!options.games) { throw new Error('missing games property'); }
    if (!(options.games instanceof EventEmitter)) { throw new Error('games must be an EventEmitter'); }

    var games = {};

    options.games.on('created', function (game) {
        games[game.id] = game;
    });
    options.games.on('terminated', function (game) {
        if (game.callbacks[0]) { game.callbacks[0](); }
        if (game.callbacks[1]) { game.callbacks[1](); }
        game.delete_at = Date.now() + 600000;
    });
    options.games.on('deleted', function (game) {
        delete games[game.id];
    });

    function checkAccess(game, ap) {
        return isUUID(game) &&
            isUUID(ap) &&
            games[game] && (
                games[game].aps[0] === ap ||
                games[game].aps[1] === ap
            );
    }

    return function (req, res, next) {
        if (checkAccess(req.params.game, req.params.ap)) {
            req.game = games[req.params.game];
            if (req.game.death) {
                return res.status(410).json({
                    error: 'Game Terminated',
                    message: req.game.death
                });
            }
            return next();
        }
        res.status(401).json({
            error: 'Unauthorized',
            message: 'Access denied to this game'
        });
    };
}

function createGameEnder(options) {
    if (!options.games) { throw new Error('missing games property'); }
    if (!(options.games instanceof EventEmitter)) { throw new Error('games must be an EventEmitter'); }

    var games = {};

    options.games.on('created', function (game) {
        games[game.id] = game;
    });
    options.games.on('terminated', function (game) {
        delete games[game.id];
    });

    return function (req, res) {
        var game = games[req.game.id];
        game.death = req.body;
        options.games.emit('terminated', game);
        res.status(200).end();
    };
}


function gameReporter(req, res) {
    var game = req.game,
        index = game.aps[0] === req.params.ap ? 0 : 1;
    game.accessed[index] = Date.now();
    if (game.data[index]) {
        res.send({
            move: game.data[index]
        });
        game.data[index] = undefined;
    } else {
        game.callbacks[index] = function () {
            game.callbacks[index] = undefined;
            if (game.death) {
                return res.status(410).json({
                    error: 'Game Terminated',
                    message: game.death
                });
            }
            res.json({
                move: game.data[index]
            });
            game.data[index] = undefined;
        };
        res.setTimeout(30000);
        res.once('timeout', function () {
            game.callbacks[index] = undefined;
        });
        req.once('aborted', function () {
            game.callbacks[index] = undefined;
        });
    }
}

function gameRunner(req, res) {
    var game = req.game;
    if (game.aps[game.turn % 2] !== req.params.ap) {
        return res.status(403).json({
            error: 'Forbidden',
            message: 'It is not your turn'
        });
    }
    if (req.headers['content-type'] !== 'text/plain') {
        return res.status(400).json({
            error: 'Malformed',
            message: 'Request content-type must be text/plain'
        });
    }
    if (req.body.length !== 5 ||
            !_.includes(['M', 'A', 'H', 'T', 'R', 'F'], req.body[0]) ||
            !_.includes(['1', '2', '3', '4', '5', '6'], req.body[1]) ||
            !_.includes(['1', '2', '3', '4', '5', '6'], req.body[2]) ||
            !_.includes(['0', '1', '2', '3', '4', '5', '6'], req.body[3]) ||
            !_.includes(['0', '1', '2', '3', '4', '5', '6'], req.body[4])) {
        return res.status(400).json({
            error: 'Malformed',
            message: 'Wrong move format'
        });
    }

    res.status(200).end();
    game.turn += 1;
    game.data[game.turn % 2] = req.body;
    if (game.callbacks[game.turn % 2]) {
        game.callbacks[game.turn % 2]();
    }
}

exports.createGameParser = createGameParser;
exports.createGameEnder = createGameEnder;
exports.gameReporter = gameReporter;
exports.gameRunner = gameRunner;
