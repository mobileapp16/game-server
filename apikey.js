/*jslint node: true, nomen: true */
"use strict";

var _ = require('lodash'),
    isUUID = require('validator').isUUID;

function ignore() { return undefined; }

function parseAuthentication(req, res, next) {
    ignore(res);
    var auth = req.get("authorization"),
        components;
    if (auth) {
        components = auth.split(' ');
        if (components.length === 2 && components[0] === 'APIKey') {
            req.apikey = components[1];
        }
    }
    next();
}

function createAPIKeyValidityChecker(access) {
    return function (req, res, next) {
        if (req.apikey && isUUID(req.apikey) && access[req.apikey]) {
            return next();
        }
        res.set('WWW-Authenticate', 'APIKey');
        res.status(401).json({
            error: 'Authorization Required'
        });
    };
}

function createAPIKeyRoomAccessChecker(access) {
    return function (req, res, next) {
        if (_.includes(access[req.apikey], req.params.room)) {
            return next();
        }
        res.status(401).json({
            error: 'Unauthorized',
            message: 'You do not have access to this room'
        });
    };
}

exports.parseAuthentication = parseAuthentication;
exports.createAPIKeyValidityChecker = createAPIKeyValidityChecker;
exports.createAPIKeyRoomAccessChecker = createAPIKeyRoomAccessChecker;
